package com.voitenko.checkweather.Providers.Net;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.voitenko.checkweather.Adapters.AdapterWeather;
import com.voitenko.checkweather.Data.AllWeather;
import com.voitenko.checkweather.Data.Marker;
import com.voitenko.checkweather.Interface.FeedbackWeather;
import com.voitenko.checkweather.Providers.Bd.WeatherProvider;
import com.voitenko.checkweather.Settings.ConstantsCW;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeaterLoader implements Callback<AllWeather> {
    private WeatherProvider weatherProvider;
    private String units = "metric";
    private String key = ConstantsCW.API_WEATHER_KEY;
    private WeatherApi.ApiInterface apiInterface;
    private long idPosition;
    private FeedbackWeather feedbackWeather;



    public WeaterLoader(Double lat, Double lng, Context context, long idPosition, FeedbackWeather feedbackWeather) throws IOException {
        apiInterface = WeatherApi.getClient().create(WeatherApi.ApiInterface.class);
        weatherProvider = new WeatherProvider(context);
        this.idPosition = idPosition;
        Call<AllWeather> call = apiInterface.getWeather(String.valueOf(lat), String.valueOf(lng), units, key);
        call.enqueue(this);
        this.feedbackWeather=feedbackWeather;
    }


    @Override
    public void onResponse(Call<AllWeather> call, Response<AllWeather> response) {
        if(call.isExecuted()){
        AllWeather allWeather = response.body();
        allWeather.setIdMarker(idPosition);
        Date currentTime = Calendar.getInstance().getTime();
        allWeather.setTime(currentTime.toString());
        weatherProvider.insetrItem(allWeather);
        if(feedbackWeather!=null){
            feedbackWeather.onFeedBackToActivity();
        }}
    }
    @Override
    public void onFailure(Call<AllWeather> call, Throwable t) { }

}
